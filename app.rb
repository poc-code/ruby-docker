#!/usr/bin/env ruby

require 'bundler'
Bundler.setup
Bundler.require(:default)

use Rack::Logger

helpers do
  def logger
    request.logger
  end
end

get('/') do
  logger.info params.inspect
  h = { json_data: 'this is a test' }
  Oj.dump h
end
