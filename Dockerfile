# vim:set ft=dockerfile:
FROM ruby:alpine

MAINTAINER Andrius Kairiukstis <andrius@kairiukstis.com>

RUN apk add --update \
      libstdc++ \
      libxml2 \
      libxslt \
      libffi \
      pcre \
      zlib \
      build-base \
      git \
      libxml2-dev \
      libxslt-dev \
      libffi-dev \
      pcre-dev \
      ruby-dev \
&& rm -rf /var/cache/apk/* /tmp/* /var/tmp/*

ENV APP_HOME /app

ENV BUNDLE_GEMFILE $APP_HOME/Gemfile
ENV BUNDLE_RETRY   5
ENV BUNDLE_JOBS    4
ENV BUNDLE_PATH    /bundle

WORKDIR ${APP_HOME}
COPY Gemfile /${APP_HOME}/
COPY Gemfile.lock /${APP_HOME}/

RUN bundle install \
&& rm -rf /usr/lib/ruby/gems/*/cache/* \
&& rm -rf /var/cache/apk/* /tmp/* /var/tmp/*

ENTRYPOINT ["bundle", "exec"]
CMD ["puma", "-C", "config/puma.rb"]
